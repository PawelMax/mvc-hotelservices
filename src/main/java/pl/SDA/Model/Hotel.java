package pl.SDA.Model;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    private List<Room> numberOfRooms = new ArrayList<Room>();

    public Hotel() {

        Room room11 = new Room(11, true);
        Room room12 = new Room(12, false);
        Room room13 = new Room(13, true);
        Room room14 = new Room(14, false);
        Room room15 = new Room(15, true);
        Room room16 = new Room(16, true);
        Room room17 = new Room(17, true);
        Room room18 = new Room(18, false);
        Room room19 = new Room(19, true);

        numberOfRooms.add(room11);
        numberOfRooms.add(room12);
        numberOfRooms.add(room13);
        numberOfRooms.add(room14);
        numberOfRooms.add(room15);
        numberOfRooms.add(room16);
        numberOfRooms.add(room17);
        numberOfRooms.add(room18);
        numberOfRooms.add(room19);
    }

    public List<Room> getNumberOfRooms() {
        return numberOfRooms;
    }


}
