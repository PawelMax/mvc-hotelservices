package pl.SDA.Model;

public class Room {

    private int roomNumber;
    private boolean isAvailable;

    public Room(int roomNumber, boolean isAvailable) { // konstruktor
        this.roomNumber = roomNumber;
        this.isAvailable = isAvailable;
    }

    public Room(int roomNumber){
        this.roomNumber = roomNumber;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(byte roomNumber) {
        this.roomNumber = roomNumber;
    }

    public boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomNumber=" + roomNumber +
                ", isAvailable=" + isAvailable +
                '}';
    }
}
