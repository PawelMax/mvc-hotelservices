package pl.SDA.Model;

import java.util.ArrayList;
import java.util.List;

// MVC - Model
public class HotelServices {

    private Hotel hotel;

    public HotelServices(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<Room> getAllRooms() {
        return hotel.getNumberOfRooms();
    }

    public void bookingRoom(int lookingForRoom) {
        for (Room room : getAllRooms()) {
            getAllRooms().get(room.getRoomNumber()).setIsAvailable(false);
        }
    }




}
