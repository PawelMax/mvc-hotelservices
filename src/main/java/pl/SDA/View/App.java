package pl.SDA.View;

import pl.SDA.Controler.UserController;
import pl.SDA.Model.Hotel;
import pl.SDA.Model.HotelServices;

public class App {
    public static void main(String[] args) {
        Hotel hotel = new Hotel();
        HotelServices hotelServices = new HotelServices(hotel);
        Menu menu = new Menu();
        new UserController(hotelServices, menu);

        menu.loadPage();
    }
}
