package pl.SDA.View;

import pl.SDA.Controler.UserController;
import pl.SDA.Model.Hotel;
import pl.SDA.Model.HotelServices;
import pl.SDA.Model.Room;

import java.util.InputMismatchException;
import java.util.Scanner;

// MVC - VIEW
public class Menu {

    private UserController controller;
    Hotel hotel = new Hotel();
    HotelServices hotelServices = new HotelServices(hotel);
    Scanner sc = new Scanner(System.in);
    int lookingForRoom;
    Room room;

    public void setController(UserController controller) {
        this.controller = controller;
    }

    private void displayMenu() {
        System.out.println("\n         Witamy w Hotelu");
        System.out.println("--------------------------------------");
        System.out.println("1 - pobierz listę wszystkich pokoi w tym hotelu");
        System.out.println("2 - pobierz listę wszystkich pokoi dostępnych");
        System.out.println("3 - zarezerwuj pokój");
        System.out.println("======================================");
        System.out.print("\n zaznacz co zamierzasz wykonać: ");
    }

    public void loadPage() {
        displayMenu();
        try {
            byte input = sc.nextByte(); // pobranie z klawiatury
            switch (input) {
                case 1:
                    showAllRooms();
                    break;
                case 2:
                    showAllAvailableRooms();
                    break;
                case 3:
                    System.out.println("podaj numer pokoju do rezerwacji");
                    lookingForRoom = sc.nextInt();
                    controller.bookingRoom(lookingForRoom);
                    break;
            }
        } catch (NumberFormatException e) {
            System.err.println("błędny parametr");
        } catch (InputMismatchException e) {
            System.err.println("błędny parametr");
        }
    }

    public void responceRoomIsNotAvailable(){ //
        System.out.printf("pokój %d został już wcześniej zarezerwowany \n",lookingForRoom);
    }
    public void responceRoomBecamebooked(){ //
        System.out.printf("pokój %d został zarezerwowany \n",lookingForRoom);
    }
    public void responceRoomIsNotExist(){ //
        System.out.printf("pokój o numerze %d nie istnieje \n",lookingForRoom);
    }
    private void showAllRooms(){
        for (Room room : controller.getAllRooms()) {
            System.out.println(room);
        }
    }
    private void showAllAvailableRooms(){
        for (Room room : controller.getAllAvailableRooms()) {
            System.out.println(room);
        }
    }

}
