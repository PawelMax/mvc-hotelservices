package pl.SDA.Controler;

import pl.SDA.Model.Hotel;
import pl.SDA.Model.HotelServices;
import pl.SDA.Model.Room;
import pl.SDA.View.Menu;

import java.util.ArrayList;
import java.util.List;

// MVC - Controller
public class UserController {

    private HotelServices hotelServices;
    private Menu menu;
    private Hotel hotel;


    public UserController(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<Room> getAllRooms() {
        return hotel.getNumberOfRooms();
    }

    public List<Room> getAllAvailableRooms() {

        List<Room> allRooms = getAllRooms(); // lista wszystkich pokoi
        List<Room> availableRooms = new ArrayList<Room>(); // lista pokoi dostępnych - nowa lista


        for (int i = 0; i < allRooms.size(); i++) {

            if (allRooms.get(i).getIsAvailable() == true) {
                availableRooms.add(allRooms.get(i));
            }
        }
        return availableRooms;
    }

    //połączenie kontrolera z modelem(hotelServices) i widokiem (menu)
    public UserController(HotelServices hotelServices, Menu menu) {
        this.hotelServices = hotelServices;
        this.menu = menu;
        this.menu.setController(this);

    }

    public void bookingRoom(int lookingForRoom) {  // metoda sprawdzająca poprawność pokoju i przekazująca do modelu ( HotelServices)

        boolean isFoundRoom = false;
        boolean isFoundAvailable = false;// tzw flaga

        for (Room room : getAllRooms()) {

            if (getAllRooms().get(room.getRoomNumber()).getRoomNumber() == lookingForRoom) {
                isFoundRoom = true;

                if (getAllRooms().get(room.getRoomNumber()).getIsAvailable() == true) {
                    isFoundAvailable = true;
                }
            }
        }
        if (isFoundRoom == true && isFoundAvailable == true) {
            menu.responceRoomBecamebooked();
            hotelServices.bookingRoom(lookingForRoom);
        } else if (isFoundRoom == true && isFoundAvailable == false) {
            menu.responceRoomIsNotAvailable();
        } else if (isFoundRoom == false) {
            menu.responceRoomIsNotExist();
        }


    }
}

